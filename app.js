const
	{app} = require('electron').remote,
	{ipcRenderer, shell, remote, BrowserWindow} = require('electron'),
	path = require('path'),
	fs = require('fs'),
	BASELINE = '<em>La muziko estas grandioza.</em>',
	savePATH = path.join(app.getPath('appData'), 'musiko'),
	VERSION = app.getVersion(), // version in package.json
	isLinux = process.platform === 'linuxgood',
	currentWindow = remote.getCurrentWindow()

//Lancement de l'application. Récupère pour cela les JSONs dans savePATH
function launchApp() {
	function getContent(filename, defaultReturn = []) {
		const p = path.join(savePATH, filename + '.json')
		if (fs.existsSync(p)) return JSON.parse(fs.readFileSync(p))
		return defaultReturn
	}
	const
		albums = getContent('collection'),
		/*lists = getContent('playlists').map(l => ({
			album: l.n,
			cover: l.cover,
			artwork: '❤',
			discs: [{
				disc: 0,
				tracks: l.t.map(arrADT => { //une piste est une réf à un num d'Album, à un num Disc et à un num Track
					let a = arrADT[0], d = arrADT[1], t = arrADT[2]
					a = albums[a]
					d = a.discs[d]
					t = d.tracks[t]
					const cloneT = { ...t } //façon de cloner un objet (spread)
					cloneT.albumRef = a
					cloneT.discRef = d
					return cloneT
				})
			}]
		})),*/
		genres = getContent('genres'),
		years = getContent('years'),
		lastAlbums = getContent('lastAlbums'),
		config = getContent('config', {})
	
	document.body.setAttribute('data-theme', config.theme)
	let a = {
		configPath: savePATH,
		language: config.language,
		theme: config.theme || 'dark',
		separator: config.separator || ';',
		click1: config.click1 || 1, //clic gauche: ajouter à la file d'attente
		click2: config.click2 || 3, //clic droit: voir les infos
		click3: config.click3 || 4, //clic milieu: sélectionner
		L: langObj(config.language),
		currentForTriple: 'Album',
		albums, lists: [], genres, years,
		lastAlbums,
		nbLastAlbums: config.nbLastAlbums,
		collectionPath: config.collectionPath,
		tracks: [],
		queue: [], past: [], sel: [],

		//donné par getAlbumsAndTracksOf(value, of) quand demandé
		byGenre: {albums: [], tracks: []},
		byYear: {albums: [], tracks: []},
		byArtist: {albums: [], tracks: []},

		defaultTitle: BASELINE,
		currentTitle: BASELINE,
		showPause: false,
		audio: undefined,
		inPan: undefined, //music in left panel {album, track, artist: []}
		currentQPlayed: undefined,
		getAlbumsAndTracksOf: function (value, of) {
			let byAlbum = [], byTrack = []
			for (const album of this.albums) {
				for (const d of album.discs) {
					for (const track of d.tracks) {
						let condition
						if (of === 'genre') {
							condition = track.genres && track.genres.indexOf(value) !== -1
						} else if (of === 'year') {
							condition = track.year && track.year === value
						} else if (of === 'artist') {
							condition = track.artists && track.artists.indexOf(value) !== -1
						}
						if (condition) byTrack.push({track, album})
					}
				}
			}
			// Réunion des pistes ayant le même album et le même artwork.
			for (const t of byTrack) {
				const i = byAlbum.findIndex(el => el.artworkName === t.album.artwork && el.albumName === t.album.album)
				if (i === -1) {
					// on ajoute un objet à byAlbum
					byAlbum.push({
						artworkName: t.album.artwork,
						albumName: t.album.album,
						tracks: [t]
					})
				} else {
					// on ajoute la piste à l'index
					byAlbum[i].tracks.push(t)
				}
			}
			// Ensuite on récupère le nombre de piste d'un album et on le compare au nombre de piste dans byAlbum. Si le compte y est, on met dans albums. S'il n'y est pas, on met dans tracks.
			let albums = [], tracks = []
			for (const a1 of byAlbum) {
				for (const a of this.albums) {
					if (a1.artworkName === a.artwork && a1.albumName === a.album) {
						let nbTracks = 0
						for (const d of a.discs) nbTracks += d.tracks.length
						if (a1.tracks.length === nbTracks) {
							albums.push(a1.tracks[0].album)
						} else {
							for (const t of a1.tracks) tracks.push(t)
						}
						continue
					}
				}
			}
			// Note : on pourrait améliorer cette fonction vu que tout est trié alphabétiquement. Si la lettre est après ce que l'on recherche, on passe à l'élément suivant.
			const obj = {albums, tracks}
			if (of === 'genre') this.byGenre = obj
			if (of === 'year') this.byYear = obj
			if (of === 'artist') this.byArtist = obj
			return obj
		},
		getCtx: function(e, elts = [], obj) {
			function getIcon(id) {
				/*
					1: Mettre en attente
					2: Jouer directement
					3: Voir les infos
					4: Afficher le menu contextuel
					5: Sélectionner / Déselectionner
				*/
				if (id === a.click1) return vm.I.mouseL
				if (id === a.click2) return vm.I.mouseR
				if (id === a.click3) return vm.I.mouseM
				return ''
			}
			if (document.getElementById('ctx')) document.body.removeChild(document.getElementById('ctx'))
			const box = document.createElement('div')
			box.id = 'ctx'
			box.classList.add('ctx')
			box.innerHTML = ''
			for (const elt of elts) {
				if (elt === 'playlist') {
					box.innerHTML += `
						<div class=item id=btRmPlaylist>
							<span>${this.L.btRmPlaylist}</span>
							<span class=icon></span>
						</div>
						<hr>
					`
				}
			}
			box.innerHTML += `
				<div class=item id=btQueue>
					<span>${this.L.btQueue}</span>
					<span class=icon>${getIcon(1)}</span>
				</div>
				<div class=item id=btDirect>
					<span>${this.L.btDirect}</span>
					<span class=icon>${getIcon(2)}</span>
				</div>
				<div class=item id=btShow>
					<span>${this.L.btShow}</span>
					<span class=icon>${getIcon(3)}</span>
				</div>
				<div class=item id=btSel>
					<span>${this.L.btSel}</span>
					<span class=icon>${getIcon(5)}</span>
				</div>
			`

			const pointer = document.createElement('div')
			
			box.appendChild(pointer)
			pointer.classList.add('ctxPointer')
			

			const wH = window.innerHeight/2
			const wW = window.innerWidth/2
			const X = e.clientX
			const Y = e.clientY
			let l = 'auto', b = 'auto', r = 'auto', t = 'auto'
			if(Y > wH && X <= wW) { //bas-gauche
				l = X + 'px'
				b = window.innerHeight - Y + 'px'
				pointer.style.left = - 12 + 'px'
				pointer.style.bottom = - 12 + 'px'
			} else if(Y > wH && X > wW) { //bas-droite
				b = window.innerHeight - Y + 'px'
				r = window.innerWidth - X + 'px'
				pointer.style.right = - 12 + 'px'
				pointer.style.bottom = - 12 + 'px'
			} else if(Y <= wH && X <= wW) { //haut-gauche
				l = X + 'px'
				t = Y + 'px'
				pointer.style.left = - 12 + 'px'
				pointer.style.top = - 12 + 'px'
			} else { //haut-droite
				r = window.innerWidth - X + 'px'
				t = Y + 'px'
				pointer.style.right = - 12 + 'px'
				pointer.style.top = - 12 + 'px'
			}
			box.style.left = l
			box.style.bottom = b
			box.style.right = r
			box.style.top = t
	
			document.body.appendChild(box)
			box.addEventListener('mouseleave', () => {
				document.body.removeChild(box)
			}, false)
			document.getElementById('btQueue').addEventListener('click', () => {
				if (obj.type === 'Album') {
					this.playAlbum(obj.value, obj.index)
				} else if (obj.type === 'Track') {
					this.playTrack({track: obj.value.track, album: obj.value.album})
				} else if (obj.type === 'Artists') {
					this.playArtists(obj.value.artists)
				}
				document.body.removeChild(box)
			}, false)
			document.getElementById('btDirect').addEventListener('click', () => {
				if (obj.type === 'Album') {
					this.playAlbumDirect(obj.value, obj.index)
				} else if (obj.type === 'Track') {
					this.playTrackDirect({track: obj.value.track, album: obj.value.album})
				} else if (obj.type === 'Artists') {
					this.playArtistsDirect(obj.value.artists)
				}
				document.body.removeChild(box)
			}, false)
			document.getElementById('btShow').addEventListener('click', () => {
				if (obj.type === 'Album') { 
					this.showAlbum(obj.value, obj.index)
				} else if (obj.type === 'Track') {
					this.showTrack(obj.value.track, obj.value.album)
				} else if (obj.type === 'Artists') {
					this.showArtists(obj.value.album, obj.value.artists)
				}
				document.body.removeChild(box)
			}, false)
			document.getElementById('btSel').addEventListener('click', () => {
				if (obj.type === 'Album') {
					this.selectAlbum(obj.value, obj.index)
					obj.component.isSelected = true
					if (obj.component.disc && obj.component.disc === 'discindexhere') {
						obj.component.disc = obj.index
					}
					setTimeout(function () {
						obj.component.isSelected = false
						if (obj.component.disc) obj.component.disc = 'discindexhere'
					}, 600)
				} else if (obj.type === 'Track') {
					this.selectTrack(obj.value.track, obj.value.album)
					obj.component.isSelected = true
					setTimeout(function () {
						obj.component.isSelected = false
					}, 600)
				} else if (obj.type === 'Artists') {
					this.selectArtists(obj.value.artists)
					obj.component.isSelectedArtists = true
					if (obj.component.artist && obj.component.artist === 'artistnamehere') {
						obj.component.artist = obj.value.artists[0]
					}
					setTimeout(function () {
						obj.component.isSelectedArtists = false
						if (obj.component.artist) obj.component.artist = 'artistnamehere'
					}, 600)
				}
				document.body.removeChild(box)
				
			}, false)
			const btSpecial1 = document.getElementById('btRmPlaylist')
			if (btSpecial1) {
				btSpecial1.addEventListener('click', () => {
					if (obj.type === 'Album') {
						this.removePlaylist(obj.index)
					}
					document.body.removeChild(box)
				}, false)
			}
		},
		shuffle: arr => arr.map(a => [Math.random(), a]).sort((a, b) => a[0] - b[0]).map(a => a[1]),
		getTime: function (t) {
			let
				h = Math.floor(t / 3600),
				m  = Math.floor((t % 3600) / 60),
				s  = Math.floor(t % 60)
			if (s < 10) s = '0' + s
			if (m < 10) m = '0' + m
			const res = (h ? h + ':' : '') + m + ':' + s
			if (res === 'NaN:NaN') return '-:-'
			return res
		},
		Sound: function () {
			if (this.queue.length === 0) {
				this.currentTitle = this.defaultTitle
				document.title = 'Musiko'
				this.audio = undefined
				this.showPause = false
				setTimeout(() => {
					document.getElementById('timeline').style.width = '0'
					document.getElementById('timelineBt').style.display = 'none'
				}, 100)
				return
			}
			const t = this.queue[0].track
			this.audio = new Audio()
			this.audio.src = this.collectionPath + t.path
			this.audio.load()
			this.currentTitle = t.title +  ' — ' + t.artists.join(', ')
			this.inPan = this.queue[0]
			this.currentQPlayed = this.queue[0]
	
			//sauvegarde de l'album des dernières écoutes
			const newAlbum = this.currentQPlayed.album
			let la = this.lastAlbums.filter(
				alb => alb.album !== newAlbum.album && alb.artwork !== newAlbum.artwork
			)
			la.unshift(newAlbum)
			la = la.slice(0, this.nbLastAlbums)
			this.lastAlbums = la
			fs.writeFileSync(path.join(savePATH, 'lastAlbums.json'), JSON.stringify(this.lastAlbums))
	
			document.title = this.currentTitle
			this.past.unshift(this.queue.shift())
	
			this.showPause = true
			this.audio.play()
			this.audio.addEventListener('ended', function() {
				a.audio = undefined
				document.getElementById('timelineBt').style.display = 'none'
				a.Sound()
			}, false)
			this.audio.addEventListener('timeupdate', function() {
				//this = audio
				if (!this) return
				let t = a.getTime(this.duration - this.currentTime)
				if (t === 'NaN:NaN') return
				if (a.progressValue !== t) {
					document.title = a.currentTitle + ' (' + t + ')'
					a.progressValue = t
				}
				const w = `calc(100% * ${this.currentTime} / ${this.duration})`
				document.getElementById('timeline').style.width = w
				document.getElementById('timelineBt').style.display = 'block'
				document.getElementById('timelineBt').style.left = w
			}, false)
		},
		playPause: function () {
			if (!this.audio && this.queue.length === 0) {
				// joue un album parmi ceux affichés, choisi aléatoirement
				const albums = this.albums
				const album = albums[Math.floor(Math.random() * albums.length)]
				for (let disc of album.discs) for (let track of disc.tracks) {
					this.queue.push({track, album})
				}
				// joue la première musique de queue
				this.Sound()
				return
			}
			if (!this.audio.paused) {
				// met en pause la lecture
				this.audio.pause()
				this.showPause = false
				return
			}
			// reprend la lecture
			this.audio.play()
			this.showPause = true
		},
		stop: function () {
			if (this.audio) {
				this.audio.pause()
				this.audio.currentTime = 0
				this.showPause = false
			}
		},
		random: function () {
			if (this.queue.length === 0) {
				// crée une queue de 50 musiques à partir de tous les albums
				for (let a of this.albums) for (let d of a.discs) for (let t of d.tracks) {
					this.queue.push({track: t, album: a})
				}
				this.queue = this.shuffle(this.queue).slice(0, 50)
				if (!this.audio) {
					// crée l'audio et le joue
					this.Sound()
					return
				}
				return
			}
			// mélange la queue existante
			this.queue = this.shuffle(this.queue)
		},
		skip: function() {
			if (this.audio) {
				this.audio.pause()
				this.Sound()
			} else if (this.queue.length > 0) {
				this.Sound()
			}
		},
		getArtists: function(album) {
			let artist = []
			for (const d of album.discs) for (const t of d.tracks) for (const a of t.artists) {
				if (artist.indexOf(a) === -1) artist.push(a)
			}
			return artist
		},
		changeInPan: function(album) {
			this.inPan = {track: album.discs[0].tracks[0], album, artist: this.getArtists(album)}
		},
		// show
		showAlbum: function(album, discIndex) {
			this.changeInPan(album)
			vm.tab = 'Album'
			this.currentForTriple = vm.tab
			if (discIndex) {
				setTimeout(() => document.getElementById('disc_'+ discIndex).scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"}), 100)
			}
		},
		showArtists: function(album, artists) {
			this.inPan = {track: album.discs[0].tracks[0], album, artist: artists}
			vm.tab = 'Artist'
			this.currentForTriple = vm.tab
		},
		showTrack: function(track, album) {
			this.inPan = {track, album}
			vm.tab = 'Track'
			this.currentForTriple = vm.tab
		},
		// play (queue)
		playAlbum: function(album, discIndex) {
			if (discIndex !== undefined) {
				for (const track of album.discs[discIndex].tracks) {
					this.queue.push({track, album})
				}
			} else {
				for (const d of album.discs) for (const track of d.tracks) {
					this.queue.push({track, album})
				}
			}
			if (!this.audio) this.Sound()
			this.changeInPan(album)
		},
		playArtists(artists) {
			for (const artist of artists) {
				for (const album of this.albums) for (const d of album.discs) for (const track of d.tracks) {
					if (track.artists.indexOf(artist) !== -1) this.playTrack({track, album})
				}
			}
		},
		playTrack: function(q) {
			this.queue.push(q)
			if (!this.audio) this.Sound()
		},
		// play direct
		playAlbumDirect: function(album, discIndex) {
			let add = []
			if (discIndex !== undefined) {
				for (let track of album.discs[discIndex].tracks) {
					add.push({track, album})
				}
			} else {
				for (let d of album.discs) {
					for (let track of d.tracks) {
						add.push({track, album})
					}
				}
			}
			add = add.reverse()
			for (const el of add) this.queue.unshift(el)
			this.skip()
			//this.changeInPan(album)
		},
		playArtistsDirect: function(artists) {
			let add = []
			for (const artist of artists) {
				for (const album of this.albums) {
					for (const disc of album.discs) {
						for (const track of disc.tracks) {
							if (track.artists.indexOf(artist) !== -1) {
								add.push({track, album})
							}
						}
					}
				}
			}
			add = add.reverse()
			for (const el of add) this.queue.unshift(el)
			this.skip()
		},
		playTrackDirect: function(q) {
			this.queue.unshift(q)
			this.skip()
		},
		// select
		selectAlbum: function(album, discIndex) {
			if (discIndex !== undefined) {
				for (const track of album.discs[discIndex].tracks) {
					this.sel.push({track, album})
				}
			} else {
				for (const d of album.discs) for (const track of d.tracks) {
					this.sel.push({track, album})
				}
			}
		},
		selectArtists: function(artists) {
			for (const artist of artists) {
				const {albums, tracks} = this.getAlbumsAndTracksOf(artist, 'artist')
				for (const album of albums) {
					for (const d of album.discs) {
						for (const track of d.tracks) {
							this.sel.push({track, album})
						}
					}
				}
				for (const t of tracks) {
					this.sel.push({track: t.track, album: t.album})
				}
			}
		},
		selectTrack: function(track, album) {
			this.sel.push({track, album})
		},
		// special
		removePlaylist: function(index) {
			this.lists.splice(index, 1)
			let data = []
			for (const l of this.lists) {
				data.push('#' + l.album, '>/' + path.basename(l.cover))
				for (const t of l.discs[0].tracks) {
					data.push(t.path.normalize())
				}
				data.push('')
			}
			const p = path.join(this.collectionPath, 'play.lists')
			if (fs.existsSync(p)) {
				fs.writeFileSync(p, data.join('\n'))
			} else {
				alert('An error was occured : "play.lists" file has been deleted.')
			}
		}
	}
	if (a.albums.length !== 0) {
		const rdmAlbum = a.shuffle(a.albums)[0]
		const rdmDisk = a.shuffle(rdmAlbum.discs)[0]
		const rdmTrack = a.shuffle(rdmDisk.tracks)[0]
		a.inPan = {track: rdmTrack, album: rdmAlbum}
	}
	const vm = new Vue({
		el: "#app",
		components: {Banner, Asidepan, Album},
		data: {
			a,
			I: icons,
			tab: "Album",

			//pour la glissière temporelle
			id: 'timelineBt',
			id2: 'newTime',
			active: false,
			inPlayed: false, //permet de savoir si la musique est en lecture ou en pause avant le glissement, pour remettre l'état à la fin du glissement
			newTime: '',
			currentX: undefined,
		},
		methods: {
			//pour la glissière temporelle

			dragStart: function(e) {
				if (e.target.id === this.id) {
					this.active = true
					document.getElementById(this.id2).style.display = 'block'
					if (!a.audio.paused) {
						this.inPlayed = true
						// met en pause la lecture
						a.audio.pause()
						a.showPause = false
					}
				}
			},
			dragEnd: function(e) {
				if (this.active) {
					/*
						durée musique		|		largeur fenêtre
						? nouveau temps		|		espace entre début et curseur
						=
						a.audio.duration	|		window.innerWidth
						?					|		this.currentX
					*/
					a.audio.currentTime = a.audio.duration * this.currentX / window.innerWidth
					document.getElementById(this.id2).style.display = 'none'
					this.active = false
					this.newTime = ''
					if (this.inPlayed) {
						// reprend la lecture
						a.audio.play()
						a.showPause = true
						this.inPlayed = false
					}
				}
			},
			drag: function(e) {
				if (this.active) {
					e.preventDefault()
					this.currentX = e.clientX
					if (this.currentX < 0) {
						this.currentX = 0
					} else if (this.currentX > window.innerWidth) {
						this.currentX = window.innerWidth
					}
					document.getElementById(this.id).style.left = this.currentX + 'px'
					let pos = this.currentX + 10
					if (pos + document.getElementById(this.id2).offsetWidth > window.innerWidth) {
						pos -= document.getElementById(this.id2).offsetWidth + 20
					}
					document.getElementById(this.id2).style.left = pos + 'px'
					this.newTime = a.getTime(a.audio.duration * this.currentX / window.innerWidth)
				}
			},

			//pour la recherche par index

			getIndexs: function () {
				let chars = []
				for (const album of a.albums) {
					const c = album.artwork.split('')[0].toUpperCase()
					if (chars.indexOf(c) === -1) chars.push(c)
				}
				return chars
			},
			goTo: function (char) {
				const elts = document.body.querySelectorAll("[data-name='artwork']")
				let go = false
				for (const e of [... elts]) {
					if (e.textContent.split('')[0].toUpperCase() === char) {
						if (!go) {
							e.parentNode.parentNode.scrollIntoView()
							go = true
						}
						e.classList.add('blink')
						const i = setInterval(() => {
							e.classList.remove('blink')
							clearInterval(i)
						}, 800)
					}
				}
			}
		}
	})
	return vm
}

// INIT : Cette fonction génère la collection en prenant en compte une ancienne config si elle est dispo. À la fin de la génération, on obtient plusieurs JSONs dans savePATH et l'app est relancée (ce qui ait qu'on revient au routage).
function initCollection(configRecup) {
	let config, isNewVersion = false
	function getLang() {
		const l = ['fr', 'de', 'it', 'ae', 'ru', 'es', 'jp', 'cn']
		const i = l.indexOf(app.getLocale().slice(0, 2))
		return i > -1 ? l[i] : 'en'
	}
	if (!configRecup && fs.existsSync(path.join(savePATH, 'config.json'))) {
		const {collectionPath, language, theme, nbLastAlbums, separator, click1, click2, click3} = JSON.parse(fs.readFileSync(path.join(savePATH, 'config.json')))
		config = {collectionPath, language, theme, nbLastAlbums, separator, click1, click2, click3}
	} else if (configRecup) {
		config = configRecup
		isNewVersion = configRecup.version !== VERSION
	} else {
		config = {
			collectionPath: app.getPath('music'),
			nbLastAlbums: 9,
			language: getLang(),
			theme: 'dark',
			separator: ';',
			click1: 1,
			click2: 3,
			click3: 4
		}
	}
	const note = `
		<h1>Updated to Musiko ${VERSION}</h1>
		<ol>
			<li>Displacement of the read head possible.</li>
			<li>Bug concerning the creation of compilations corrected.</li>
			<li>Addition of the Separator parameter (semi-colon by default).</li>
			<li>Delete a compilation.</li>
			<li>Customizable actions.</li>
			<li>Minor bugs fixed.</li>
		</ol>
		<p><em>The release notes are accessible via the Settings panel, by clicking on the current version number.</em></p>
		<div class=bt id=ok>OK</div>
	`
	config.version = VERSION
	document.body.setAttribute('data-theme', config.theme)
	const L = langObj(config.language)
	const splashload = document.createElement('div')
	splashload.classList.add('splashload')
	splashload.innerHTML = L.creationCollection + `<div id=loader></div>`
	if (isNewVersion) splashload.innerHTML += `<div class=versionNote id=vn>${note}</div>`
	document.body.appendChild(splashload)
	const loader = document.getElementById('loader')
	if (isNewVersion) {
		document.getElementById('ok').addEventListener('click', () => {
			isNewVersion = false
			splashload.removeChild(document.getElementById('vn'))
		})
	}
	ipcRenderer.on('info' , (e, data) => {
		loader.innerHTML = data.nb + ' / ' + data.total + ' ' + L.musiquesAnalysees
	})
	ipcRenderer.on('finish', (e, data) => {
		if (isNewVersion && document.getElementById('ok')) {
			//c'est une nouvelle version. L'app ne se relancera que si on clique sur OK.
			document.getElementById('ok').addEventListener('click', () => currentWindow.reload())
		} else currentWindow.reload()
	})
	ipcRenderer.send('initCollection', config, savePATH)
}

// ROUTAGE : Si on a un config.json, on lance l'app normalement, sinon on initialise la collection
let vm
if (fs.existsSync(path.join(savePATH, 'config.json'))) {
	const configRecup = JSON.parse(fs.readFileSync(path.join(savePATH, 'config.json')))
	if (configRecup.version !== VERSION) { // Maj détectée
		fs.rmdirSync(savePATH, { recursive: true })
		initCollection(configRecup)
		//vm = launchApp()
	} else {
		vm = launchApp()
	}
} else initCollection()

// ROUTAGE DE LA LANGUE
function langObj(lang) {
	if (lang === 'ae') return ae
	if (lang === 'cn') return cn
	if (lang === 'de') return de
	if (lang === 'en') return en
	if (lang === 'eo') return eo
	if (lang === 'es') return es
	if (lang === 'fr') return fr
	if (lang === 'it') return it
	if (lang === 'jp') return jp
	if (lang === 'ru') return ru
}

// Récup des playlists
ipcRenderer.on('receiveLists', (e, data, app) => {
	vm.a.lists = data.lists
	if (data.logSyntax.length > 0 || data.logPath.length > 0) {
		const logListsPath = path.join(app.collectionPath, 'play.lists.log')
		alert(`Error(s) in play.lists file. Show "${logListsPath}" for more details.`)
	}
})

// SAISIE AU CLAVIER : ouverture du panneau Search
document.addEventListener('keydown', function (e) {
	if (e.which === 112) {
		window.open('https://bitbucket.org/diatomee/musiko/src/master/README.md', '_blank', 'nodeIntegration=no')
	} else if ([8,9,13,17,18,20,27,37,38,40,45,46,113,114,115,116,117,118,119,120,121,122,123,225].indexOf(e.which) !== -1) { //touches qui ne sont ni chiffre, ni lettre, ni ponctuation
		//on ne fait rien
	} else {
		if (e.target.tagName === 'BODY' && vm) {
			vm.tab = 'Search'
			setTimeout(() => { //le setTimeout sert a rendre existant iSearch, autrement ça ne marche pas (ce qui est un peu bizarre, cela veut dire que le composant a besoin d'un petit temps pour se charger dans le DOM, même s'il est keep-alive)
				const input = document.getElementById('iSearch')
				input.focus()
				input.value = e.key
			}, 1)
		}
	}
})