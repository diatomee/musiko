# Versions

## future (> 1.0.8)

**FR**

1. Modification (ordre des pistes, changement de titre et d'image, suppression de pistes) et suppression d'une compilation depuis Musiko.
2. Rubrique Dernières écoutes sous forme de pistes.
3. Édition des métadonnées depuis Musiko.
4. Ajout de métadonnées (humeurs, label et métadonnées personnalisées).
5. Ajout de vidéos (clips, concerts...).
6. Filtrage, compilations aléatoires.
7. ...

## 1.0.8 (current / actuelle)

**EN**

1. Displacement of the read head possible.
2. Bug concerning the creation of compilations corrected.
3. Addition of the Separator parameter (semi-colon by default).
4. Minor bugs fixed.
5. Add tracks in playlists.
6. Delete a compilation.
7. Customizable actions.

**FR**

1. Déplacement de la tête de lecture possible.
2. Bug concernant la création de compilations corrigé.
3. Ajout du paramètre Séparateur (point-virgule par défaut).
4. Bugs mineurs corrigés.
5. Insertion de pistes dans une compilation.
6. Suppression d'une compilation.
7. Actions personnalisables.

## 1.0.7

**EN**

1. New panel: Mixing (regroups for the moment the compilations and the last plays).
2. Creation of compilations (playlists) from Musiko.
3. No need to refresh the collection to update the compilations.
4. Merger of the Album, Track and Artist panels.
5. Move the search for works by index (bottom right, on hover).
6. Improved actions + selection (to create a compilation).
7. Updated documentation.

**FR**

1. Nouveau panneau : Mélange (regroupe pour le moment les compilations et les dernières écoutes).
2. Création des compilations (playlists) depuis Musiko.
3. Plus besoin de rafraîchir la collection pour mettre à jour les compilations.
4. Fusion des panneaux Album, Piste et Artiste.
5. Déplacement de la recherche d'oeuvres par index (en bas à droite, au survol).
+6. Actions améliorées + sélection (pour créer une compilation).
7. Documentation mise à jour.


Édition des métadonnées dans un fichier texte :

Id - Id
Titre - Title
Artistes - Artists
Genres - Genres
Année - Year
Mots-clés - Keywords
Références - References
Album



## 1.0.6

**EN**

1. Choice of 10 themes to change the look.
2. New panel: Artists.
3. Improved research.
4. Panel removal: Filters.
5. Various bugs fixed.
6. Adding this small update window ;)

**FR**

1. Choix parmi 10 thèmes pour changer l'apparence.
2. Nouveau panneau : Artistes.
3. Amélioration de la recherche.
4. Suppression du panneau : Filtres.
5. Divers bogues corrigés.
6. Ajout de cette petite fenêtre de mise à jour ;)

## 1.0.5

**FR**

1. La création de la collection musicale est plus rapide et se fait jusqu'au bout.
2. Si la langue de l'utilisateur est indisponible, l'anglais sera mis par défaut.
3. Parmi plusieurs pochettes disponibles dans le dossier ou le sous-dossier où se trouve la musique, le choix se portera prioritairement sur :
	1. cover.[ext]
	2. folder.[ext]
	3. front.[ext]
	4. [max-file-size].[ext]
4. Lors d'une mise à jour, la collection n'est plus rafraîchit systématiquement.
5. Recherche améliorée :
	- saisir ":" + lettre = accéder à l'index dans la liste des albums
	- commencer à saisir du texte n'importe où pour débuter une recherche
	- les résultats de recherche sont cliquables directement pour être joués ou observés.
