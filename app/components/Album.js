const Album = {
	props: {
		app: Object,
		album: Object, // an album
		goto: Boolean, // add HTML data-name=artwork to navigate by index
		concise: Boolean, // concise the album name and displays a max of 2 artists
		ctxAlbum: Array,
		ctxArtist: Array,
		index: Number
	},
	data: function () {
		return {
			src: 'app/img/noCover.svg',
			isHover: false,
			isSelected: false,
			isSelectedArtists: false
		}
	},
	created: function() {
		this.getSrc()
	},
	updated: function() {
		this.getSrc()
	},
	template: `
	<div class=padding>
		<div
			@mouseover=toggleHover
			@mouseout=toggleHover
			:class="{hover: isHover, selected: isSelected}"
			@click.exact=rootClick($event,1)
			@click.right.exact=rootClick($event,2)
			@click.alt.exact=rootClick($event,3)
			@click.middle.exact=rootClick($event,3)
		>
			<img :src=src>
			<div style="font-size:1.2em;margin-bottom:2px;margin-top:-2px;">
				<b v-html=getConcise></b>
			</div>
		</div>
		<span
			class=miniBt
			:class="{selected: isSelectedArtists}"
			v-for="el of artworkAndArtists"
			v-html=el.text
			@click.exact=rootClick2($event,1,el.artists)
			@click.right.exact=rootClick2($event,2,el.artists)
			@click.alt.exact=rootClick2($event,3,el.artists)
			@click.middle.exact=rootClick2($event,3,el.artists)
		></span>
	</div>
	`,
	computed: {
		nbTracks: function() { //non utilisé (donne le nombre de pistes de l'album)
			let i = 0
			for (const d of this.album.discs) i += d.tracks.length
			return i
		},
		path: function() {
			if (!this.album.cover) return 'app/img/noCover.svg'
			return this.album.cover
		},
		artworkAndArtists: function() {
			let arr = []
			const artwork = this.album.artwork
			for (let d of this.album.discs) for (let t of d.tracks) for (let a of t.artists) {
				if (arr.findIndex(el => el.text === a) === -1) arr.push({text: a, artists: [a]})
			}
			if (arr.length === 1 && arr[0].text === artwork) {
				//on a un seul artiste dans arr et il se trouve que c'est le nom de l'oeuvre
				if (!this.goto) return [{text: artwork, artists: [artwork]}]
				return [{
					text: `<span data-name=artwork>${artwork}</span>`,
					artists: [artwork]
				}]
			}
			if (this.concise && arr.length > 2) {
				const newArr = arr.slice(2)
				let artists = []
				for (const el of newArr) {
					for (const a of el.artists) artists.push(a)
				}
				arr = [arr[0], arr[1], {text: '…', artists}]
			}
			arr.unshift({
				text: `<b ${this.goto ? ' data-name=artwork' : ''}>${artwork}</b>`,
				artists: arr.map(el => el.artists[0])
			})
			return arr
		},
		getConcise: function() {
			if (!this.concise) return this.album.album
			if (this.album.album.length < 38) return this.album.album
			const a = this.album.album
			return a.substring(0, 35).trim() + '…'
		}
	},
	methods: {
		async getSrc() {
			if (this.album.cover) {
				//setTimeout(function() {
					this.src = this.album.cover
				//}.bind(this), 200)
			}
		},
		rootClick: function(e, nb) {
			/*
				1: Mettre en attente
				2: Jouer directement
				3: Voir les infos
				4: Afficher le menu contextuel
				5: Sélectionner / Déselectionner
			*/
			const clk = this.app['click' + nb]
			if (clk === 1) this.app.playAlbum(this.album)
			else if (clk === 2) this.app.playAlbumDirect(this.album)
			else if (clk === 3) this.app.showAlbum(this.album)
			else if (clk === 4) this.app.getCtx(e, this.ctxAlbum, {
				type: 'Album',
				value: this.album,
				index: this.index,
				component: this
			})
			else if (clk === 5) this.toggleSelected()
		},
		rootClick2: function(e, nb, artists) {
			const clk = this.app['click' + nb]
			if (clk === 1) this.app.playArtists(artists)
			else if (clk === 2) this.app.playArtistsDirect(artists)
			else if (clk === 3) this.app.showArtists(this.album, artists)
			else if (clk === 4) this.app.getCtx(e, this.ctxArtist, {
				type: 'Artists',
				value: {album: this.album, artists},
				component: this
			})
			else if (clk === 5) this.toggleSelectedArtists(artists)
		},
		toggleHover: function() {this.isHover = !this.isHover},
		toggleSelected: function() {
			this.app.selectAlbum(this.album)
			this.isSelected = true
			setTimeout(function () {
				this.isSelected = false
			}.bind(this), 600)
		},
		toggleSelectedArtists: function(artists) {
			this.app.selectArtists(artists)
			this.isSelectedArtists = true
			setTimeout(function () {
				this.isSelectedArtists = false
			}.bind(this), 600)
		}
	}
}