const AsideAlbum = {
	props: {app: Object},
	data: function () {
		return {isSelected: false}
	},
	components: {Album, Tracks},
	template: `
		<div v-if=app.inPan class=panel>
			<Album :app=app :album=app.inPan.album style="padding:20px;"></Album>
			<div v-for="d, index of app.inPan.album.discs" :id="'disc_'+index">
				<div
					v-if="d.disc && app.inPan.album.discs.length > 1"
					@click.exact=rootClick($event,1,index)
					@click.alt.exact=rootClick($event,3,index)
					@click.middle.exact=rootClick($event,3,index)
					:class="{selected: isSelected === index}"
				>
					<div class="title1 toggle">
						<div v-if=d.disctitle :title="app.L.disque + ' ' + d.disc">{{d.disctitle}}</div>
						<div v-else>{{app.L.disque}} {{d.disc}}</div>
					</div>
					<img
						v-if=d.disccover
						:src=d.disccover
						style="cursor: pointer; padding-left:20px;"
					>
				</div>
				<Tracks :app=app :tracks=tracks(d.tracks)></Tracks>
			</div>
		</div>
		<div v-else class=panel>
			<div style="padding: 20px;"><em>{{app.L.pasAlbum}}</em></div>
		</div>
	`,
	methods: {
		rootClick: function(e, nb, index) {
			/*
				1: Mettre en attente
				2: Jouer directement
				3: Voir les infos
				4: Afficher le menu contextuel
				5: Sélectionner / Déselectionner
			*/
			const clk = this.app['click' + nb]
			const album = this.app.inPan.album
			if (clk === 1) this.app.playAlbum(album, index)
			else if (clk === 2) this.app.playAlbumDirect(album, index)
			else if (clk === 3) this.app.showAlbum(album, index)
			else if (clk === 4) this.app.getCtx(e, [], {
				type: 'Album',
				value: album,
				index,
				component: this
			})
			else if (clk === 5) this.toggleSelectedDisc(index)
		},
		toggleSelectedDisc: function(index) {
			this.app.selectAlbum(this.app.inPan.album, index)
			this.isSelected = index
			setTimeout(function () {
				this.isSelected = false
			}.bind(this), 600)
		},
		tracks: function(tracks) {
			return tracks.map(track => ({track, album: this.app.inPan.album}))
		}
	}
}