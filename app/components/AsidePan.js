const Asidepan = {
	props: {app: Object, icon: Object},
	components: {AsideQueue, AsideSel, AsideMixer, AsideSearch, AsideAlbum, AsideTrack, AsideArtist, AsideParams},
	mounted: function () {
		ipcRenderer.send('getLists', this.app)
	},
	template: `
	<aside>
		<nav :class=nbNavIcons>
			<div
				:class="{active: $root.tab === 'Queue'}"
				@click="$root.tab = 'Queue'"
				v-if="app.queue.length > 0 || app.past.length > 0"
				v-html=icon.queue
			></div>
			<div
				:class="{active: $root.tab === 'Sel'}"
				@click="$root.tab = 'Sel'"
				v-if="app.sel.length > 0"
				v-html=icon.selection
			></div>
			<div
				:class="{active: $root.tab === 'Mixer'}"
				@click="$root.tab = 'Mixer'"
				v-html=icon.mixer
			></div>
			<div
				:class="{active: $root.tab === 'Search'}"
				@click="$root.tab = 'Search'"
				v-html=icon.search
			></div>
			<div
				:class="{active: $root.tab === 'Album' || $root.tab === 'Track' || $root.tab === 'Artist'}"
				@click=nextTab
				@wheel=nextTab
				v-html="icon[app.currentForTriple.toLowerCase() + '2']"
			></div>
			<div
				:class="{active: $root.tab === 'Params'}"
				@click="$root.tab = 'Params'"
				v-html=icon.params
			></div>
		</nav>

		<AsideQueue v-if="$root.tab === 'Queue'" :app=app></AsideQueue>
		<keep-alive><AsideSel v-if="$root.tab === 'Sel'" :app=app></AsideSel></keep-alive>
		<keep-alive><AsideMixer v-if="$root.tab === 'Mixer'" :app=app></AsideMixer></keep-alive>
		<keep-alive><AsideSearch v-if="$root.tab === 'Search'" :app=app></AsideSearch></keep-alive>
		<AsideAlbum v-if="$root.tab === 'Album'" :app=app></AsideAlbum>
		<AsideTrack v-if="$root.tab === 'Track'" :app=app></AsideTrack>
		<AsideArtist v-if="$root.tab === 'Artist'" :app=app></AsideArtist>
		<AsideParams v-if="$root.tab === 'Params'" :app=app></AsideParams>
	</aside>
	`,
	computed: {
		nbNavIcons: function() {
			let nb = 4
			if (this.app.queue.length > 0 || this.app.past.length > 0) nb++
			if (this.app.sel.length > 0) nb++
			return 'nbNavIcons' + nb
		}
	},
	methods: {
		nextTab: function() {
			if (['Track', 'Artist', 'Album'].indexOf(this.$root.tab) === -1) {
				this.$root.tab = this.app.currentForTriple
			}
			else if (this.app.currentForTriple === 'Album') this.$root.tab = 'Track'
			else if (this.app.currentForTriple === 'Track') this.$root.tab = 'Artist'
			else if (this.app.currentForTriple === 'Artist') this.$root.tab = 'Album'
			this.app.currentForTriple = this.$root.tab
		}
	}
}
