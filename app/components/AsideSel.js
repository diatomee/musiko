const AsideSel = {
	props: {app: Object},
	components: {Album, Tracks},
	data: function () {
		return {
			opened: 'selection',
			namePlaylist: this.app.L.exempleCompil,
			over: false,
			coverSrc: false,
			coverName: false,
			coverExt: false,
			errCover: false
		}
	},
	template: `
		<div class=panel>
			<span
				@click=deselectAll
				v-html=$root.I.clear
				class=btClear
			></span>
			<div class="title1 toggle" @click="changeOpened('create playlist')">
				<div
					class="icon noLeft"
					:class="{rotate: opened === 'create playlist'}"
					v-html=$root.I.fold
				></div>
				<div v-html=app.L.creerCompil></div>	
			</div>
			<div v-if="opened === 'create playlist'" class=padding>
				<input v-model=namePlaylist>
				<input style="display:none;" type=file id=filepicker accept="image/*" @change=changePath>
				<div v-if=coverSrc class=coverAreaWithCover>
					<div v-html=$root.I.clear @click=resetCover class=clearCover></div>
					<img :src=coverSrc style="width:100%;">
					<input v-model=coverName @input="errCover=false" style="display:inline-block; width:88%; font-size:.9em;"><span style="font-size:.8em;padding-left:4px;">{{coverExt}}</span>
				</div>
				<div v-else
					@dragover=dragover($event)
					@dragleave=dragleave
					@drop=drop($event)
					@click=openChooser
					class=coverArea
					:class="{hover: over}"
					v-html=app.L.choisirCouv
				></div>
				<div v-if=errCover class=errCover v-html=errCover></div>
				<div v-if="namePlaylist.trim() !== '' && coverName && coverName.trim() !== ''" class=bt v-html=app.L.creer @click=createPlaylist></div>
			</div>
			<div class="title1 toggle" @click="changeOpened('add to a playlist')">
				<div
					class="icon noLeft"
					:class="{rotate: opened === 'add to a playlist'}"
					v-html=$root.I.fold
				></div>
				<div v-html=app.L.ajouterACompil></div>
			</div>
			<div v-if="opened === 'add to a playlist'">
				<div v-for="album,i of app.lists">
					<div class=searchResult @click=insertInPlaylist(i)>
						<img style="width:22%;margin-right:.5em;" :src=album.cover>
						<div>
							<div>{{album.album}}</div>
							<div class=align>
								<span v-html=nbTracks(album)></span>
								<span v-html=$root.I.album></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="title1 toggle" @click="changeOpened('selection')">
				<div
					class="icon noLeft"
					:class="{rotate: this.opened === 'selection'}"
					v-html=this.$root.I.fold
				></div>
				<div>{{app.L.selection}} ({{app.sel.length}})</div>	
			</div>
			<div v-if="opened === 'selection'">
				<Tracks :app=app :tracks=app.sel :removeIn=app.sel removeInName=sel></Tracks>
			</div>
		</div>
	`,
	methods: {
		nbTracks: function(album) {
			let nb = 0
			for (const d of album.discs) nb += d.tracks.length
			return nb
		},
		resetCover: function() {
			this.coverSrc = false
			this.coverName = false
			this.coverExt = false
		},
		openChooser: function() {
			document.getElementById('filepicker').click()
		},
		changePath: function (e) {
			if (e.target.files.length === 0) return
			this.getCover(e.target.files[0])
		},
		dragover: function(e) {
			this.over = true
			e.preventDefault();
			e.dataTransfer.dropEffect = 'copy';
			return false;
		},
		dragleave: function() {
			this.over = false
			return false
		},
		drop: function(e) {
			this.over = false
			e.preventDefault()
			if (e.dataTransfer.files.length === 0) return
			this.getCover(e.dataTransfer.files[0])
			return false
		},
		getCover: function (f) {
			console.log(f.type)
			if (f.type.split('/')[0] === 'image') {
				//on a bien une image
				const ext = path.extname(f.name)
				this.coverSrc = f.path
				this.coverName = path.basename(f.name, ext)
				this.coverExt = ext
			} else alert(`Only image file. ${f.type !== '' ? f.type : 'This file' } is not accepted.`)
		},
		createPlaylist: function() {
			const { COPYFILE_EXCL } = fs.constants
			try {
				const dest = path.join(this.app.collectionPath, this.coverName.trim() + this.coverExt)
				if (path.dirname(this.coverSrc) === this.app.collectionPath) {
					//l'image est déjà dans le dossier musical
					// 2 cas se présentent : l'image a le même nom : on ne fait rien, l'image n'a pas le même nom, on la duplique
					const ext = path.extname(this.coverSrc)
					if (this.coverName !== path.basename(this.coverSrc, ext)) {
						fs.copyFileSync(this.coverSrc, dest, COPYFILE_EXCL)
					}
				} else { //l'image n'est pas dans le dossier musical
					fs.copyFileSync(this.coverSrc, dest, COPYFILE_EXCL)
				}
				let data = ['', '']
				data.push('#' + this.namePlaylist.trim())
				data.push('>/' + this.coverName.trim() + this.coverExt)
				for (const t of this.app.sel) data.push(t.track.path.normalize())
				data = data.join('\n')
				const p = path.join(this.app.collectionPath, 'play.lists')
				fs.existsSync(p) ? fs.appendFileSync(p, data) : fs.writeFileSync(p, data)
				ipcRenderer.send('getLists', this.app)
				this.$root.tab = 'Mixer'
				this.app.sel = []
				this.opened = 'selection'
				this.namePlaylist = this.app.L.exempleCompil
				this.over = false
				this.coverSrc = false
				this.coverName = false
				this.coverExt = false
				this.errCover = false
			} catch (err) {
				this.errCover = this.app.L.erreurImg
			}
		},
		insertInPlaylist: function(i) {
			for (const t of this.app.sel) {
				const cloneT = { ...t.track } //façon de cloner un objet (spread)
				cloneT.albumRef = t.album
				for (const d of t.album.discs) {
					for (const tr of d.tracks) {
						if (tr.path === t.track.path) {
							cloneT.discRef = d
							break
						}
					}
				}
				this.app.lists[i].discs[0].tracks.push(cloneT)
			}
			let data = []
			for (const l of this.app.lists) {
				data.push('#' + l.album, '>/' + path.basename(l.cover))
				for (const t of l.discs[0].tracks) {
					data.push(t.path.normalize())
				}
				data.push('')
			}
			const p = path.join(this.app.collectionPath, 'play.lists')
			if (fs.existsSync(p)) {
				fs.writeFileSync(p, data.join('\n'))
			} else {
				alert('An error was occured: "play.lists" file has been deleted.')
			}
		},
		changeOpened: function(text) {
			if (text === this.opened) {
				this.opened = ''
				return
			}
			this.opened = text
		},
		deselectAll: function() {
			this.app.sel = []
			this.$root.tab = this.app.currentForTriple
		}
	}
}
