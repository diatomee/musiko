const AsideTrack = {
	props: {app: Object},
	components: {Album},
	data: function () {
		return {showOut: false}
	},
	template: `
		<div v-if=app.inPan class=panel>
			<div class=title1>{{app.inPan.track.title}}</div>
			<div class=list v-if=app.inPan.track.track>
				<span v-html=$root.$data.I.tnum></span>
				<span>{{app.inPan.track.track}}</span>
			</div>
			<div class=list>
				<span v-html=$root.$data.I.artist></span>
				<span>{{ app.inPan.track.artists.join(', ') }}</span>
			</div>
			<div class=list v-if=app.inPan.track.year>
				<span v-html=$root.$data.I.tyear></span>
				<span>{{ app.inPan.track.year }}</span>
			</div>
			<div class=list>
				<span v-html=$root.$data.I.tduration></span>
				<span>{{app.getTime(app.inPan.track.duration)}}</span>
			</div>
			<div class=list v-if="app.inPan.track.genres && app.inPan.track.genres.length > 0">
				<span v-html="$root.$data.I.tgenres"></span>
				<span>{{app.inPan.track.genres.join(', ')}}</span>
			</div>
			<div class=list>
				<span v-html=$root.$data.I.tpath></span>
				<span class="bt btPath" @click=openFolder(app.inPan.track.path)>{{app.inPan.track.path}}</span>
			</div>
			<div v-if=app.inPan.track.lyrics class=padding>
				<div @click=toggleShowOut class=bt style="margin-top: 10px; padding:10px 6px;" v-html=$root.I.lyrics></div>
			</div>
			<div v-if=showOut>
				<div class=lyrics v-html=lyrics></div>
				<div class=btClose @click=toggleShowOut v-html=$root.I.clear></div>
			</div>
			<br>
			<Album :app=app :album=app.inPan.album></Album>
		</div>
		<div v-else class="panel padding"><em>{{app.L.pasAlbum}}</em></div>
	`,
	computed: {
		size: function() {
			return Number.parseFloat(this.app.inPan.track.filesize / 1000000).toFixed(2)
		},
		lyrics: function () {
			return this.app.inPan.track.lyrics.replace(/\n/g, '<br>') + '<div style="height:50px;"></div>'
		}
	},
	methods: {
		openFolder: function (musicPath) {
			shell.showItemInFolder(this.app.collectionPath + '/' + musicPath)
		},
		toggleShowOut: function () {
			this.showOut = !this.showOut
		}
	}
}
