const AlbumInList = {
	props: {
		app: Object,
		album: Object,
		find: String //get <b>text found</b>: 'no', 'album', 'artwork', 'both (album+artwork)'
	},
	data: function () {
		return {
			isSelected: false
		}
	},
	template: `
	<div
		class=searchResult
		@click.exact=rootClick($event,1)
		@click.right.exact=rootClick($event,2)
		@click.alt.exact=rootClick($event,3)
		@click.middle.exact=rootClick($event,3)
		:class="{selected: isSelected}"
	>
		<img style="width:22%;margin-right:.5em;" :src=album.cover>
		<div>
			<div v-html=getBoldAlbum></div>
			<div v-html=getBoldArtwork></div>
		</div>
	</div>
	`,
	computed: {
		getBoldAlbum: function() {
			return this.find === 'album' || this.find === 'both'
				? `<b>${this.album.album}</b>`
				: this.album.album
		},
		getBoldArtwork: function() {
			return this.find === 'artwork' || this.find === 'both'
				? `<b>${this.album.artwork}</b>`
				: this.album.artwork
		}
	},
	methods: {
		rootClick: function(e, nb) {
			/*
				1: Mettre en attente
				2: Jouer directement
				3: Voir les infos
				4: Afficher le menu contextuel
				5: Sélectionner / Déselectionner
			*/
			const clk = this.app['click' + nb]
			if (clk === 1) this.app.playAlbum(this.album)
			else if (clk === 2) this.app.playAlbumDirect(this.album)
			else if (clk === 3) this.app.showAlbum(this.album)
			else if (clk === 4) this.app.getCtx(e, [], {
				type: 'Album',
				value: this.album,
				component: this
			})
			else if (clk === 5) this.toggleSelected()
		},
		toggleSelected: function() {
			this.app.selectAlbum(this.album)
			this.isSelected = true
			setTimeout(function () {
				this.isSelected = false
			}.bind(this), 600)
		},
	}
}
const Albums = {
	props: {
		app: Object,
		albums: Array,
		find: String //get <b>text found</b>: 'no', 'album', 'artwork', 'both (album+artwork)'
	},
	components: {AlbumInList},
	template: `
	<div>
		<AlbumInList v-for="a, i of albums" :key="'album'+i" :app=app :album=a :find=find></AlbumInList>
	</div>
	`
}