const Banner = {
	props: {app: Object},
	data: function () {
		return {
			isStopped: false,
		}
	},
	template: `
		<header>
			<div class=topbar v-if=!$root.active>
				<div class=ctrls>
					<span class=icon @click=playPause v-html="app.showPause ? $root.$data.I.pause : $root.$data.I.play"></span>
					<span :class="{btDisabled: isDisabled1}" class=icon @click=stop v-html="$root.$data.I.replay"></span>
					<span class=icon @click=random v-html="$root.$data.I.rand"></span>
					<span :class="{btDisabled: isDisabled2}" class=icon @click=skip v-html=$root.$data.I.skip></span>
				</div>
				<span class=playing v-html=app.currentTitle @click=showTrack></span>
				<div class="icon iconRight" @click=refresh title="Rafraîchir la collection" v-html=$root.$data.I.refresh :title=app.collectionPath></div>
				<div
					v-if="app.queue.length > 0"
					class=indicatorQueue
					@click=clear
					:title=app.L.vider
				>
					<transition name=fade>
						<span :key=app.queue.length>{{app.queue.length}}</span>
					</transition>
				</div>
			</div>
			<div class=topbar v-else></div>
			<div class=timeline id=timeline></div>
			<div
				class=timelineBt
				id=timelineBt
			></div>
			<div class=newTime id=newTime v-html=$root.newTime></div><!--j'en suis là: l'id sert à déplacer la div en même temps que le curseur et la classe à la styler. Les 2 sont à faire. Le v-html est aussi à faire.-->
		</header>
	`,
	computed: {
		isDisabled1: function () {
			return this.app.currentTitle === this.app.defaultTitle || this.isStopped
		},
		isDisabled2: function () {
			return this.app.queue.length === 0 && this.app.currentTitle === this.app.defaultTitle
		}
	},
	methods: {
		playPause: function() {
			this.app.playPause()
			if (this.isStopped) this.isStopped = false
		},
		stop: function () {
			this.app.stop()
			this.isStopped = true
		},
		random: function () {
			this.app.random()
		},
		skip: function () {
			this.app.skip()
			if (this.isStopped) this.isStopped = false
		},
		showTrack: function () {
			if (!this.app.currentQPlayed) return
			this.app.inPan = this.app.currentQPlayed
			this.$root.tab = 'Track'
			this.app.currentForTriple = this.$root.tab
		},
		refresh: function () {
			initCollection()
		},
		clear: function () {
			this.app.queue = []
		}
	}
}
