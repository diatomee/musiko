const AsideQueue = {
	props: {app: Object},
	data: function () {
		return {isHover: false}
	},
	components: {Tracks},
	template: `
		<div class=panel>
			<div v-if="app.queue.length === 0" style="text-align:center; padding:20px;">
				<em v-html=app.L.fileAttenteVide></em>
			</div>
			<div v-else>
				<div class=title1>
					{{nbQueue}}<br>
					<span class=btClear @click=clear :title=app.L.vider v-html=$root.$data.I.clear></span>
				</div>
				<Tracks :app=app :tracks=tracks(app.queue) :removeIn=app.queue :removeInName="'queue'"></Tracks>
			</div>
			<div v-if="app.past.length === 0" style="text-align:center; padding:20px;">
				<em v-html=app.L.historiqueVide></em>
			</div>
			<div v-else>
				<div class=title1>
					{{nbPast}}
					<span class=btClear @click=clear2 :title=app.L.vider v-html=$root.$data.I.clear></span>
				</div>
				<Tracks :app=app :tracks="tracks(app.past)" :removeIn=app.past :removeInName="'past'"></Tracks>
			</div>
		</div>
	`,
	computed: {
		nbQueue: function () {
			const nb = this.app.queue.length
			let d = 0
			for (let q of this.app.queue) d += q.track.duration
			const time = this.app.getTime(d)
			return this.app.L.pisteAttente(nb, time)
		},
		nbPast: function () {
			const nb = this.app.past.length
			return this.app.L.pisteLancee(nb)
		}
	},
	methods: {
		clear: function() {
			this.app.queue = []
			if (this.app.past.length === 0) {
				this.$root.tab = this.app.currentForTriple
			}
		},
		clear2: function() {
			this.app.past = []
			if (this.app.queue.length === 0) {
				this.$root.tab = this.app.currentForTriple
			}
		},
		tracks: function(tracks) {
			return tracks.map(t => ({track: t.track, album: t.album}))
		}
	}
}
