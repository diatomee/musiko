const Track = {
	props: {
		app: Object,
		track: Object,
		album: Object,
		search: Boolean,
		cover: Boolean,
		removeIn: Array,
		removeInName: String,
		ctxTrack: Array
	},
	data: function () {
		return {
			isSelected: false
		}
	},
	template: `
		<div
			class=searchResult
			@click.exact=rootClick($event,1,track,album)
			@click.right.exact=rootClick($event,2,track,album)
			@click.alt.exact=rootClick($event,3,track,album)
			@click.middle.exact=rootClick($event,3,track,album)
			:class="{selected: isSelected, read: isRead, twoCol: (track.albumRef && track.albumRef.cover), remove: removeIn}"
		>
			<div v-if=isRead class=pointerRead></div>
			<img v-if="track.albumRef && track.albumRef.cover" style="width:15%;margin-right:.5em;" :src=track.albumRef.cover>
			<img v-else-if="cover && album.cover" style="width:15%;margin-right:.5em;" :src=album.cover>
			<div>
				<b v-if=search>{{track.title}}</b>
				<div v-else>{{track.title}}</div>
				<div class=less>{{track.artists.join(', ')}}&nbsp;· {{app.getTime(track.duration)}}</div>
			</div>
		</div>
	`,
	computed: {
		isRead: function () {
			return this.app.past.length > 0
				&& this.app.past[0].track.title === this.track.title
				&& this.app.past[0].track.track === this.track.track
				&& this.app.past[0].track.duration === this.track.duration
		}
	},
	methods: {
		rootClick: function(e, nb, track, album) {
			/*
				1: Mettre en attente
				2: Jouer directement
				3: Voir les infos
				4: Afficher le menu contextuel
				5: Sélectionner
			*/
			const clk = this.app['click' + nb]
			if (clk === 1) {
				if (!this.removeIn) this.app.playTrack({track, album})
				else this.rm({track, album}, this.removeIn)
			}
			else if (clk === 2) this.app.playTrackDirect({track, album})
			else if (clk === 3) this.app.showTrack(track, album)
			else if (clk === 4) this.app.getCtx(e, this.ctxTrack, {
				type: 'Track', value: {track, album}, component: this
			})
			else if (clk === 5) this.toggleSelected(track, album)
		},
		rm: function (q, arr) {
			arr.splice(arr.findIndex(e => e.track.path === q.track.path), 1)
			if (this.removeInName === 'queue') {
				if (this.app.queue.length === 0 && this.app.past.length === 0) {
					this.$root.tab = this.app.currentForTriple
				}
			} else if (this.removeInName === 'past') {
				if (this.app.queue.length === 0 && this.app.past.length === 0) {
					this.$root.tab = this.app.currentForTriple
				}
			} else if (this.removeInName === 'sel') {
				if (this.app.sel.length === 0) {
					this.$root.tab = this.app.currentForTriple
				}
			}
		},
		toggleSelected: function(track, album) {
			this.app.selectTrack(track, album)
			this.isSelected = true
			setTimeout(function () {
				this.isSelected = false
			}.bind(this), 600)
		},
	}
}
const Tracks = {
	props: {app: Object, tracks: Array, search: Boolean, removeIn: Array, removeInName: String},
	//une track a la forme {track, album}
	components: {Track},
	template: `
	<div>
		<Track
			v-for="t, i of tracks"
			:key="'track' + i"
			:app=app
			:track=t.track
			:album=t.album
			:search=search
			:removeIn=removeIn
			:removeInName=removeInName
			:cover=cover(tracks)
		></Track>
	</div>
	`,
	methods: {
		cover: function(tracks) {
			if (this.$root.tab !== 'Album') return true
			let covers = []
			for (const t of tracks) {
				if (covers.indexOf(t.album.cover) === -1) {
					covers.push(t.album.cover)
				}
			}
			return covers.length > 1 //si plus d'une couverture alors on veut les voir, sinon, c'est que c'est celle de l'album*/
		}
	}
}