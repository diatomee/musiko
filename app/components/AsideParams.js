const AsideParams = {
	props: {app: Object},
	data: function () {
		return {
			version: VERSION,
			selectedLang: this.app.language,
			options: [
				{text: 'العربية', value: 'ae'},
				{text: '简体中文', value: 'cn'},
				{text: 'Deutsch', value: 'de'},
				{text: 'English', value: 'en'},
				{text: 'Esperanto', value: 'eo'},
				{text: 'Español', value: 'es'},
				{text: 'Français', value: 'fr'},
				{text: 'Italiano', value: 'it'},
				{text: '日本の', value: 'jp'},
				{text: 'русский', value: 'ru'}
			],
			options2: [
				{text: 'Dark', value: 'dark'},
				{text: 'Deep blue (dark)', value: 'deepblue'},
				{text: 'Forest (dark)', value: 'forest'},
				{text: 'Purple (dark)', value: 'purple'},
				{text: 'Neon (dark)', value: 'neon'},
				{text: 'Greyscale (light)', value: 'greyscale'},
				{text: 'Sand (light)', value: 'sand'},
				{text: 'Sail (light)', value: 'sail'},
				{text: 'Opaline (light)', value: 'opaline'},
				{text: 'Amaranth (light)', value: 'amaranth'}
			],
			optionsClick: [
				{text: this.app.L.btQueue, value: 1},
				{text: this.app.L.btDirect, value: 2},
				{text: this.app.L.btShow, value: 3},
				{text: this.app.L.btCtx, value: 4},
				{text: this.app.L.btSel, value: 5}
			],
			optionsSeparator: [
				{text: ';', value: ';'},
				{text: ',', value: ','},
				{text: '\\', value: '\\'},
				{text: '\\\\', value: '\\\\'},
				{text: '|', value: '|'}
			]
		}
	},
	template: `
		<div class=panel>
			<div class=title1 v-html=app.L.dossierMusical></div>
			<div class=padding>
				<input style="display:none;" type=file id=filepicker name=fileList webkitdirectory @change=changePath>
				<div class=bt @click=openDial v-html=app.L.choisirDossier></div>
			</div>

			<div class=title1 v-html=app.L.language></div>
			<div class=padding>
				<select v-model=selectedLang @change=changeLang>
					<option v-for="option in options" v-bind:value=option.value>{{option.text}}</option>
				</select>
			</div>

			<div class=title1 v-html=app.L.theme></div>
			<div class=padding>
				<select v-model=app.theme @change=changeTheme>
					<option v-for="option in options2" v-bind:value=option.value>{{option.text}}</option>
				</select>
			</div>

			<div class=title1 v-html=app.L.separateur></div>
			<div class=padding>
				<p v-html=app.L.infoSeparateur style="font-style: italic"></p>
				<select v-model=app.separator @change=changeSeparator>
					<option v-for="option in optionsSeparator" v-bind:value=option.value>{{option.text}}</option>
				</select>
			</div>
			
			<div class=title1 v-html="app.L.action"></div>
			<div class=padding>
				<!--<div v-html=app.L.actions></div>-->
				
				<div v-html=$root.I.mouseL></div>
				<select v-model=app.click1 @change=changeConfigFile>
					<option v-for="option in optionsClick" v-bind:value="option.value">{{option.text}}</option>
				</select>
				<br><br>
				<div><span v-html=$root.I.mouseM class=icon></span> (Alt / ⌥ + <span v-html=$root.I.mouseL class=icon></span>)</div>
				<select v-model=app.click3 @change=changeConfigFile>
					<option v-for="option in optionsClick" v-bind:value=option.value>{{option.text}}</option>
				</select>
				<br><br>
				<div><span v-html=$root.I.mouseR class=icon></span></div>
				<select v-model=app.click2 @change=changeConfigFile>
					<option v-for="option in optionsClick" v-bind:value="option.value">{{option.text}}</option>
				</select>
			</div>
			
			<div class=title1 v-html=app.L.nbAlbums></div>
			<div class=padding>
				<input type=number v-model=app.nbLastAlbums @change=changeConfigFile min=0 max=27>
			</div>

			<div class=title1 v-html=app.L.informations></div>
			<div class=padding>
				<p>{{app.L.dossierMusical}}&nbsp;:<br><span class="bt btPath" @click=openFolder(app.collectionPath)>{{app.collectionPath}}</span></p>
				<p>{{app.L.dossierConfig}}&nbsp;:<br><span class="bt btPath" @click=openFolder(app.configPath)>{{app.configPath}}</span></p>
				<p>{{app.L.version}}&nbsp;:<br><a href="https://bitbucket.org/diatomee/musiko/src/master/VERSIONS.md" class="bt btPath">{{version}}</a></p>
				<p>{{app.L.aide}}&nbsp;:<br><strong>F1</strong> - <a href="https://bitbucket.org/diatomee/musiko/" class="bt btPath">https://bitbucket.org/diatomee/musiko/</a></p>
			</div>
		</div>
	`,
	methods: {
		openFolder: function (path) {
			shell.openItem(path)
		},
		changeConfigFile: function() {
			const conf = {
				collectionPath: this.app.collectionPath,
				language: this.app.language,
				theme: this.app.theme,
				nbLastAlbums: this.app.nbLastAlbums,
				separator: this.app.separator,
				click1: this.app.click1,
				click2: this.app.click2,
				click3: this.app.click3,
				version: VERSION
			}
			fs.writeFileSync(path.join(savePATH, 'config.json'), JSON.stringify(conf))
			return conf
		},
		changePath: function(e) {
			const
				aDirName = path.dirname(e.target.files[0].path),
				childFolder = path.dirname(e.target.files[0].webkitRelativePath).split(path.sep)[0],
				splitPath = aDirName.split(path.sep)
				index = splitPath.indexOf(childFolder)

			let realPath = [], i = 0
			while (i <= index) {
				realPath.push(splitPath[i])
				i++
			}
			realPath = realPath.join('/')

			this.app.collectionPath = realPath
			//changer aussi dans le json puis rafraichir la collection
			const config = this.changeConfigFile()
			/*
			fs.unlinkSync(path.join(savePATH, 'collection.json'))
			fs.unlinkSync(path.join(savePATH, 'genres.json'))
			fs.unlinkSync(path.join(savePATH, 'lastAlbums.json'))
			fs.unlinkSync(path.join(savePATH, 'playlists.json'))
			fs.unlinkSync(path.join(savePATH, 'years.json'))
			*/
			initCollection(config)
		},
		changeLang: function() {
			this.app.language = this.selectedLang
			this.app.L = langObj(this.selectedLang)
			this.changeConfigFile()
			this.optionsClick = [
				{text: this.app.L.btQueue, value: 1},
				{text: this.app.L.btDirect, value: 2},
				{text: this.app.L.btShow, value: 3},
				{text: this.app.L.btCtx, value: 4},
				{text: this.app.L.btSel, value: 5}
			]
		},
		changeTheme: function() {
			this.changeConfigFile()
			document.body.setAttribute('data-theme', this.app.theme)
		},
		changeSeparator: function() {
			const config = this.changeConfigFile()
			initCollection(config)
		},
		openDial: function() {
			document.getElementById('filepicker').click()
		}
	}
}
