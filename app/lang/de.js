const de = {
    creationCollection: 'Musiksammlung erstellen.',
    musiquesAnalysees: 'Musik analysiert.',
    pasAlbum: 'Kein Album gefunden. Der Pfad zum Musikordner kann in den Einstellungen geändert werden.',
    //panneau recherche
    oeuvreParIndex: 'Arbeit nach Index', // TODO What does this mean?
    rechercheGlobale: 'Globale Suche',
    resultats: 'Ergebnisse :',
    recherche: 'Name des Albums, Werkes, Interpreten oder Titels.',
    //panneau album
    disque: 'Platte',
    //panneau file d'attente
    fileAttenteVide: 'Die Warteschlange ist leer.',
    vider: 'Leeren',
    historiqueVide: 'Keine zuletzt gespielten Titel.',
    pisteAttente: (nb, time) => `${nb} Titel verbleibend (${time}).`,
    pisteLancee: nb => `${nb === 1 ? 'Ein' : nb} Titel bereits gestartet.`,
    //panneau filtres
    genres: 'Genres',
    annees: 'Jahre',
    retirerFiltres: 'Alle Filter entfernen',
    //panneau params
    dossierMusical: 'Musikordner',
    dossierConfig: 'Konfigurationsordner',
    choisirDossier: 'Ordner wählen',
    informations: 'Informationen',
    version: 'Version',
    aide: 'Dokumentation',
    language: 'Sprache',
    theme: 'Aussehen',
    nbAlbums: 'Anzahl der Alben vom letzten Hören',
    action: 'Aktionen',
    actions: '<b>Linksklick</b>: Abspielen, Halten oder Spezialaktion<br><b>Mittelklick oder Strg + Klick</b>: Wählen Sie (nützlich zum Erstellen einer Zusammenstellung)<br><b>Rechtsklick</b>: Weitere Informationen anzeigen',
    separateur: 'Trennzeichen',
    infoSeparateur: 'Mit dem Trennzeichen können Sie mehrere Künstler und Genres in den Metadaten definieren.',
    //panneau sel
    exempleCompil: 'Meine Wiedergabeliste',
    creerCompil: 'Wiedergabeliste erstellen',
    creer: 'Erstellen',
    choisirCouv: 'Cover wählen oder hierher ziehen',
    selection: 'Auswahl',
    erreurImg: 'Bildname bereits vorhanden. Sie müssen es ändern (direkt unter dem Bild).',
    ajouterACompil: 'Zur Wiedergabeliste hinzufügen',
    //panneau mixer
    compilations: 'Wiedergabelisten',
    dernieresEcoutes: 'Letzte Spiele',
    //menu contextual
    btQueue: 'Schlange stellen',
    btDirect: 'Spielen Sie direkt',
    btShow: 'Siehe Informationen',
    btCtx: 'Kontextmenü',
    btSel: 'Zur Auswahl hinzufügen',
    btRmPlaylist: 'Wiedergabeliste löschen'
}
