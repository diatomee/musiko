const ae = {
    creationCollection: '.إنشاء المجموعة الموسيقية',
    musiquesAnalysees: '.الموسيقى تحليلها',
    pasAlbum: 'لم يتم العثور على ألبوم. يمكن تغيير المسار إلى مجلد الموسيقى في الإعدادات',
    //panneau recherche
    oeuvreParIndex: 'العمل حسب الفهرس',
    rechercheGlobale: 'البحث العالمي',
    resultats: ':النتائج',
    recherche: '.اسم الألبوم أو العمل أو الفنان أو المسار',
    //panneau album
    disque: 'القرص',
    //panneau file d'attente
    fileAttenteVide: '.قائمة الانتظار فارغة',
    vider: 'إفراغ',
    historiqueVide: '.التاريخ فارغ',
    pisteAttente: (nb, time) => `${nb > 1 ? 'المسارات' + ' المعلقة' + nb : 'مسار واحد معلق'} (${time})`,
    pisteLancee: nb => nb === 1 ? '.مسار تم إطلاقه بالفعل' : nb + ' .المسارات بدأت بالفعل',
    //panneau filtres
    genres: 'الأنواع الموسيقية',
    annees: 'سنوات',
    retirerFiltres: 'إزالة جميع المرشحات',
    //panneau params
    dossierMusical: 'سجل الموسيقية',
    dossierConfig: 'مجلد التكوين',
    choisirDossier: 'اختيار مجلد',
    informations: 'معلومات',
    version: 'نسخة',
    aide: 'توثيق',
    language: 'لغة',
    theme: 'المظهر',
    nbAlbums: 'عدد الألبومات من آخر يستمع',
    action: 'أجراءات',
    actions: 'النقر بزر الماوس الأيسر: تشغيل أو تعليق أو إجراء خاص <br> النقر الأوسط أو Alt / Option + النقر: تحديد (مفيد لإنشاء تجميع) <br> النقر بزر الماوس الأيمن: عرض مزيد من المعلومات',
    separateur: 'فاصل',
    infoSeparateur: 'يسمح لك الفاصل بتعريف العديد من الفنانين والعديد من الأنواع في البيانات الوصفية.',
    //panneau sel
    exempleCompil: 'بلدي قائمة التشغيل',
    creerCompil: 'إنشاء قائمة تشغيل',
    creer: 'خلق',
    choisirCouv: 'اختر غطاء أو اسحبه هنا',
    selection: 'اختيار',
    erreurImg: 'اسم الصورة موجود بالفعل. يجب عليك تغييره (أسفل الصورة مباشرة).',
    ajouterACompil: 'أضف إلى قائمة التشغيل',
    //panneau mixer
    compilations: 'تأليف',
    dernieresEcoutes: 'آخر مسرحيات',
    //menu contextual
    btQueue: 'ضع على قائمة الانتظار',
    btDirect: 'العب مباشرة',
    btShow: 'انظر المعلومات',
    btCtx: 'قائمة سياقية',
    btSel: 'أضف إلى الاختيار',
    btRmPlaylist: 'حذف قائمة التشغيل'
}
