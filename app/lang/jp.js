const jp = {
    creationCollection: '音楽コレクションの作成。',
    musiquesAnalysees: '分析された音楽。',
    pasAlbum: 'アルバムが見つかりません。 音楽フォルダへのパスは、設定で変更できます。',
    //panneau recherche
    oeuvreParIndex: 'インデックスで作業する',
    rechercheGlobale: 'グローバル検索',
    resultats: '結果：',
    recherche: 'アルバム、作品、アーティスト、またはトラックのタイトル名。',
    //panneau album
    disque: 'ディスク',
    //panneau file d'attente
    fileAttenteVide: 'キューは空です。',
    vider: '空',
    historiqueVide: '履歴は空です。',
    pisteAttente: (nb, time) => `${nb > 1 ? '保留中のトラック': '保留中の'+nb+'トラック'}（${time}）。`,
    pisteLancee: nb => nb === 1 ? 'すでに発売されたトラック。' : nb + ' 既に発売されたトラック。',
    //panneau filtres
    genres: '音楽ジャンル',
    annees: '年',
    retirerFiltres: 'すべてのフィルターを削除',
    //panneau params
    dossierMusical: 'ミュージカルレコード',
    dossierConfig: '設定フォルダ',
    choisirDossier: 'フォルダーを選択',
    informations: '情報',
    version: 'ムシコバージョン',
    aide: 'ドキュメンテーション',
    language: '言語',
    theme: '外観',
    nbAlbums: '最後に聴いたアルバムの数',
    action: '行動',
    actions: '<b>左クリック</b>：再生、キューに入れる、または特別なアクション<br><b>中クリックまたはAlt/Option +クリック</b>：選択（編集の作成に便利）<br><b>右 クリック</b>：詳細を表示',
    separateur: 'セパレーター',
    infoSeparateur: 'セパレーターを使用すると、メタデータで複数のアーティストと複数のジャンルを定義できます。',
    //panneau sel
    exempleCompil: '私のプレイリスト',
    creerCompil: 'プレイリストを作成する',
    creer: '作成する',
    choisirCouv: 'カバーを選択するか、ここにドラッグしてください',
    selection: '選択',
    erreurImg: '画像名はすでに存在しています。 変更する必要があります（写真のすぐ下）。',
    ajouterACompil: 'プレイリストに追加する',
    //panneau mixer
    compilations: 'プレイリスト',
    dernieresEcoutes: '最後の演劇',
    //menu contextual
    btQueue: 'キューに入れる',
    btDirect: '直接プレイ',
    btShow: '情報を見る',
    btCtx: 'コンテキストメニュー',
    btSel: '選択に追加',
    btRmPlaylist: 'プレイリストを削除'
}
