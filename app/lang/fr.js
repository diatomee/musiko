const fr = {
    creationCollection: 'Création de la collection musicale.',
    musiquesAnalysees: 'musiques analysées.',
    pasAlbum: 'Aucun album trouvé. Le chemin vers le dossier des musiques peut être changé dans les paramètres.',
    //panneau recherche
    oeuvreParIndex: 'Œuvre par index',
    rechercheGlobale: 'Recherche globale',
    resultats: 'Résultats :',
    recherche: 'Nom d’album, d’œuvre, de disque, d’artiste ou de titre de piste <span class=less>(3 caractères minimum)</span>.',
    //panneau album
    disque: 'Disque',
    //panneau file d'attente
    fileAttenteVide: 'La file d’attente est vide.',
    vider: 'Vider',
    historiqueVide: 'L’historique est vide.',
    pisteAttente: (nb, time) => `${nb} piste${nb > 1 ? 's' : ''} en attente (${time}).`,
    pisteLancee: nb => nb === 1 ? 'Une piste déjà lancée.' : nb + ' pistes déjà lancées.',
    //panneau filtres
    genres: 'Genres',
    annees: 'Années',
    retirerFiltres: 'Retirer tous les filtres',
    //panneau params
    dossierMusical: 'Dossier musical',
    dossierConfig: 'Dossier de configuration',
    choisirDossier: 'Choisir un dossier',
    informations: 'Informations',
    version: 'Version de Musiko',
    aide: 'Documentation',
    language: 'Langue',
    theme: 'Apparence',
    nbAlbums: 'Nombre d’albums des dernières écoutes',
    action: 'Actions',
    actions: '<b>Clic gauche</b>: Jouer, mettre en attente ou action spéciale<br><b>Clic du milieu ou Alt/Option + clic</b>: Sélectionner (utile pour créer une compilation)<br><b>Clic droit</b>: Voir plus d’informations',
    separateur: 'Séparateur',
    infoSeparateur: 'Le séparateur permet de définir plusieurs artistes et plusieurs genres dans les métadonnées.',
    //panneau sel
    exempleCompil: 'Ma compilation',
    creerCompil: 'Créer une compilation',
    creer: 'Créer',
    choisirCouv: 'Choisir une image<br>ou la glisser ici',
    selection: 'Sélection',
    erreurImg: 'Nom d’image déjà présent. Il faut le changer (juste sous l’image).',
    ajouterACompil: 'Ajouter à une compilation',
    //panneau mixer
    compilations: 'Compilations',
    dernieresEcoutes: 'Dernières écoutes',
    //menu contextual
    btQueue: 'Mettre en attente',
    btDirect: 'Jouer directement',
    btShow: 'Voir les informations',
    btCtx: 'Menu contextuel',
    btSel: 'Ajouter à la sélection',
    btRmPlaylist: 'Supprimer la compilation'
}
