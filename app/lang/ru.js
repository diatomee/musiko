const ru = {
    creationCollection: 'Создание музыкальной коллекции.',
    musiquesAnalysees: 'музыка анализируется.',
    pasAlbum: 'Альбом не найден. Путь к папке с музыкой можно изменить в настройках.',
    //panneau recherche
    oeuvreParIndex: 'Работа по индексу',
    rechercheGlobale: 'Глобальный поиск',
    resultats: 'Результаты:',
    recherche: 'Название альбома, работы, исполнителя или трека.',
    //panneau album
    disque: 'диск',
    //panneau file d'attente
    fileAttenteVide: 'Очередь пуста.',
    vider: 'очистить',
    historiqueVide: 'История пуста.',
    pisteAttente: (nb, time) => `${nb} ${nb > 1 ? 'трекa' : 'трек'} в ожидающий (${time}).`,
    pisteLancee: nb => nb === 1 ? '1 Трек уже запущен.' : nb + ' треки уже запущены.',
    //panneau filtres
    genres: 'музыкальные жанры',
    annees: 'лет',
    retirerFiltres: 'Удалить все фильтры',
    //panneau params
    dossierMusical: 'музыкальная запись',
    dossierConfig: 'Папка конфигурации',
    choisirDossier: 'Выберите запись',
    informations: 'информация',
    version: 'Версия Musiko',
    aide: 'документация',
    language: 'язык',
    theme: 'внешность',
    nbAlbums: 'Количество альбомов с последнего прослушивания',
    action: 'действия',
    actions: '<b>Левый клик</b>: воспроизведение, постановка в очередь или специальное действие <br><b>Средний клик или Alt/Option + щелчок</b>: выберите (полезно для создания компиляции) <br><b>Справа нажмите</b>: Подробнее',
    separateur: 'сепаратор',
    infoSeparateur: 'Разделитель позволяет определять нескольких исполнителей и несколько жанров в метаданных.',
    //panneau sel
    exempleCompil: 'Мой плейлист',
    creerCompil: 'Создать плейлист',
    creer: 'Создайте',
    choisirCouv: 'Выберите обложку или перетащите ее сюда',
    selection: 'выбор',
    erreurImg: 'Название изображения уже присутствует. Вы должны изменить его (чуть ниже картинки).',
    ajouterACompil: 'Добавить в плейлист',
    //panneau mixer
    compilations: 'Плейлисты',
    dernieresEcoutes: 'Последние пьесы',
    //menu contextual
    btQueue: 'Поставить в очередь',
    btDirect: 'Играть напрямую',
    btShow: 'Смотрите информацию',
    btCtx: 'Контекстное меню',
    btSel: 'Добавить к выбору',
    btRmPlaylist: 'Удалить плейлист'
}
