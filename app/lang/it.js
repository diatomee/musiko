const it = {
    creationCollection: 'Creazione della collezione musicale.',
    musiquesAnalysees: 'musica analizzata.',
    pasAlbum: 'Nessun album trovato. Il percorso della cartella della musica può essere modificato nelle impostazioni.',
    //panneau recherche
    oeuvreParIndex: 'Lavora per indice',
    rechercheGlobale: 'Ricerca globale',
    resultats: 'risultati:',
    recherche: 'Nome del titolo dell’album, dell’opera, dell’artista o del brano.',
    //panneau album
    disque: 'Disco',
    //panneau file d'attente
    fileAttenteVide: 'La coda è vuota.',
    vider: 'svuotare',
    historiqueVide: 'La storia è vuota',
    pisteAttente: (nb, time) => `${nb} tracc${nb > 1 ? 'e' : 'ia'} in sospeso (${time}).`,
    pisteLancee: nb => nb === 1 ? 'Una traccia già lanciata.' : nb + ' tracce già lanciate.',
    //panneau filtres
    genres: 'Generi musicali',
    annees: 'Anni',
    retirerFiltres: 'Rimuovi tutti i filtri',
    //panneau params
    dossierMusical: 'Disco musicale',
    dossierConfig: 'Cartella di configurazione',
    choisirDossier: 'Scegli uno disco',
    informations: 'informazioni',
    version: 'Versione Musiko',
    aide: 'Documentazione',
    language: 'Lingua',
    theme: 'Aspetto',
    nbAlbums: 'Numero di album degli ultimi ascolti',
    action: 'Azioni',
    actions: '<b>Clic sinistro</b>: Riproduci, metti in coda o azione speciale<br><b>Clic centrale o Alt/Option + clic</b>: Seleziona (utile per creare una compilation)<br><b>Destra fai clic</b>: visualizza ulteriori informazioni',
    separateur: 'Separatore',
    infoSeparateur: 'Il separatore consente di definire diversi artisti e diversi generi nei metadati.',
    //panneau sel
    exempleCompil: 'La mia playlist',
    creerCompil: 'Crea una playlist',
    creer: 'Creare',
    choisirCouv: 'Scegli una copertina o trascinala qui',
    selection: 'Selezione',
    erreurImg: 'Nome dell’immagine già presente. Devi cambiarlo (proprio sotto l’immagine).',
    ajouterACompil: 'Aggiungi a playlist',
    //panneau mixer
    compilations: 'Playlist',
    dernieresEcoutes: 'Ultimi spettacoli teatrali',
    //menu contextual
    btQueue: 'Metti in coda',
    btDirect: 'Gioca direttamente',
    btShow: 'Vedi informazioni',
    btCtx: 'Menu contestuale',
    btSel: 'Aggiungi alla selezione',
    btRmPlaylist: 'Elimina playlist'
}
