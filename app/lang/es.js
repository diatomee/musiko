const es = {
    creationCollection: 'Creación de la colección musical.',
    musiquesAnalysees: 'música analizada.',
    pasAlbum: 'No se ha encontrado ningún álbum. La ruta a la carpeta de música se puede cambiar en la configuración.',
    //panneau recherche
    oeuvreParIndex: 'Trabajar por indice',
    rechercheGlobale: 'Búsqueda global',
    resultats: 'Resultados:',
    recherche: 'Nombre del título del álbum, obra, artista o pista.',
    //panneau album
    disque: 'Disco',
    //panneau file d'attente
    fileAttenteVide: 'La cola está vacía.',
    vider: 'Vaciar',
    historiqueVide: 'La historia está vacía.',
    pisteAttente: (nb, time) => `${nb} pista${nb > 1 ? 's' : ''} pendiente (${time}).`,
    pisteLancee: nb => nb === 1 ? 'Una pista ya lanzada.' : nb + ' pistas ya lanzadas.',
    //panneau filtres
    genres: 'Tipos',
    annees: 'Años',
    retirerFiltres: 'Eliminar todos los filtros',
    //panneau params
    dossierMusical: 'Carpeta de musica',
    dossierConfig: 'Carpeta de configuración',
    choisirDossier: 'Elige una carpeta',
    informations: 'Información',
    version: 'Versión',
    aide: 'Documentación',
    language: 'Lengua',
    theme: 'Apariencia',
    nbAlbums: 'Número de álbumes de las últimas escuchas',
    action: 'Comportamiento',
    actions: '<b>Clic izquierdo</b>: Reproducir, poner en cola o acción especial<br><b>Clic central o Alt/Option + clic</b>: Seleccionar (útil para crear una compilación)<br><b>Derecha clic</b>: ver más información',
    separateur: 'Separador',
    infoSeparateur: 'El separador le permite definir varios artistas y varios géneros en los metadatos.',
    //panneau sel
    exempleCompil: 'Mi lista de reproducción',
    creerCompil: 'Crea una lista de reproducción',
    creer: 'Crear',
    choisirCouv: 'Elige una portada o arrástrala aquí',
    selection: 'Selección',
    erreurImg: 'Nombre de la imagen ya presente. Tienes que cambiarlo (justo debajo de la imagen).',
    ajouterACompil: 'Agregar a la lista de reproducción',
    //panneau mixer
    compilations: 'Listas de reproducción',
    dernieresEcoutes: 'Ultimas jugadas',
    //menu contextual
    btQueue: 'Poner en la cola',
    btDirect: 'Jugar directamente',
    btShow: 'Ver informaciones',
    btCtx: 'Menú contextual',
    btSel: 'Agregar a la selección',
    btRmPlaylist: 'Eliminar lista de reproducción'
}
